const fs = require("fs")

if (fs.existsSync("./demo.sast.js")) {
  let start = new Date()

  // ==================================
  //  Severity:
  //  Critical, High, Medium
  // ==================================
  let severity = "Critical"
  
  // ==================================
  //  Confidence:
  //  Unknown Confirmed Experimental
  // ==================================
  let confidence = "Confirmed"
  
  // ==================================
  //  Scanner's info
  // ==================================
  let scannerName = "GitLab"
  let scannerId = "gitlab"
  let scannerSrc = "https://gitlab.com"
  let scannerVendor = "GitLab"
  let scannerVersion = "1.2.7"
  
  // ==================================
  //  Vulnerabilities' list
  // ==================================
  let vulnerabilities = [
    {
      category: "sast",
      name: `io code injection`,
      message: `🔴 vulnerability: code injection detected`,
      description: `🖐️ vulnerability: code injection detected`,
      cve: "000001",
      severity: "Critical", // Critical, High, Medium
      confidence: "Confirmed", // Unknown Confirmed Experimental
      scanner: {id: scannerId, name: scannerName},
      location: {
        file: "./demo.sast.js", 
        start_line: 3,
        end_line: 4,
        class: "owasp",
        method: "generic",
        dependency: {package: {}}
      },
      identifiers: [
        {
          type: "code_injection_rule_id",
          name: "code injection rule cwe",
          value: "owasp code injection"
        }
      ]
    },
    {
      category: "sast",
      name: `sql code injection`,
      message: `🟠 vulnerability: possible SQL code injection detected`,
      description: `🖐️ vulnerability: possible SQL code injection detected`,
      cve: "000002",
      severity: "Critical", // Critical, High, Medium
      confidence: "Confirmed", // Unknown Confirmed Experimental
      scanner: {id: scannerId, name: scannerName},
      location: {
        file: "./demo.sast.js", 
        start_line: 7,
        end_line: 9,
        class: "owasp",
        method: "generic",
        dependency: {package: {}}
      },
      identifiers: [
        {
          type: "sql_code_injection_rule_id",
          name: "sql code injection rule cwe",
          value: "sql owasp code injection"
        }
      ]
    }                    
                
  ]
  
  let end = new Date() - start
  
  // ==================================
  //  SAST Report
  // ==================================
  let sastReport = {
    version: "3.0",
    vulnerabilities: vulnerabilities,
    remediations: [],
    scan: {
      scanner: {
        id: scannerId,
        name: scannerName,
        url: scannerSrc,
        vendor: {
          name: scannerVendor
        },
        version: scannerVersion
      },
      type: "sast",
      start_time: start,
      end_time: end,
      status: "success"
    }
  }
  
  fs.writeFileSync("./gl-sast-report.json", JSON.stringify(sastReport, null, 2))
  
}

