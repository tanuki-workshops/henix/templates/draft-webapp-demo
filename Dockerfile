FROM node:13.12-slim
COPY . .
RUN npm install
CMD [ "node", "index.js" ]
